FROM registry.gitlab.com/rollingsoftware/nginx-php:latest

COPY . /var/www/
WORKDIR /var/www/
RUN apt install -y php8.1-curl
RUN php composer.phar install
RUN chown -R www-data:www-data /var/www