<?php

return [
    'default' => 'web',
    'migrations' => 'migrations',
    'connections' => [
        'web' => [
            'driver' => env('DB_WEB_CONNECTION'),
            'host' => env('DB_WEB_HOST'),
            'database' => env('DB_WEB_DATABASE'),
            'username' => env('DB_WEB_USERNAME'),
            'password' => env('DB_WEB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'createDatabaseIfNotExist' => true,
        ],
        'login' => [
            'driver' => env('DB_LOGIN_CONNECTION'),
            'host' => env('DB_LOGIN_HOST'),
            'database' => env('DB_LOGIN_DATABASE'),
            'username' => env('DB_LOGIN_USERNAME'),
            'password' => env('DB_LOGIN_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'game' => [
            'driver' => env('DB_GAME_CONNECTION'),
            'host' => env('DB_GAME_HOST'),
            'database' => env('DB_GAME_DATABASE'),
            'username' => env('DB_GAME_USERNAME'),
            'password' => env('DB_GAME_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
    ]
];