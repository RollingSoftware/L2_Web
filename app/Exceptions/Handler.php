<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        Log::error(get_class($exception) . " [" . $exception->getCode() . "] - " . $exception->getMessage());

        if ($exception instanceof ValidationException) {
            return Redirect::back()->withErrors($exception->validator->getMessageBag());
        }

        if(env('APP_ENV') != 'local') {
            if ($exception instanceof NotFoundHttpThrowable) {
                return redirect()->route('error', ['locale' => App::getLocale(), 'code' => Response::HTTP_NOT_FOUND]);
            }

            if ($exception instanceof Throwable) {

                return redirect()->route('error', ['locale' => App::getLocale(), 'code' => Response::HTTP_INTERNAL_SERVER_ERROR]);
            }
        }

        return parent::render($request, $exception);
    }
}
