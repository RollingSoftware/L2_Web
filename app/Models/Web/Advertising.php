<?php

namespace App\Models\Web;


use Illuminate\Database\Eloquent\Model;


class Advertising extends Model
{

    protected $connection = 'web';

    public $timestamps = false;
    public $table = "advertising";
    
    protected $fillable = ['name', 'view', 'registered', 'unique_view'];

}
