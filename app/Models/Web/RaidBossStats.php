<?php

namespace App\Models\Web;


use Illuminate\Database\Eloquent\Model;


class RaidBossStats extends Model
{

    protected $connection = 'web';

    public $timestamps = false;
    public $table = "raidboss_stats";

    public function rb()
    {
        return $this->belongsTo('App\Models\Game\RaidBossSpawnList', 'boss_id', 'boss_id');
    }
}
