<?php

namespace App\Models\Web;


use Illuminate\Database\Eloquent\Model;


class RaidBossStat extends Model
{

    protected $connection = 'web';

    public $timestamps = false;
    public $table = "raidboss_stats";

}
