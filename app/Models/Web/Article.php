<?php

namespace App\Models\Web;


use Illuminate\Database\Eloquent\Model;


class Article extends Model
{

    protected $connection = 'web';

    public $timestamps = false;
    public $table = "articles";

}
