<?php

namespace App\Models\Web;


use Illuminate\Database\Eloquent\Model;


class PasswordReset extends Model
{

    CONST ACTIVE = 0;
    CONST USED = 1;

    protected $connection = 'web';

    public $table = "password_resets";

    protected $primaryKey = 'username';

    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = ['email', 'token', 'status', 'username'];

}
