<?php

namespace App\Models\Game;


use Illuminate\Database\Eloquent\Model;


class CustomVariable extends Model
{

    CONST MAX_ONLINE = 'MaxOnline';

    protected $connection = 'game';

    public $timestamps = false;
    public $table = "custom_variables";


}
