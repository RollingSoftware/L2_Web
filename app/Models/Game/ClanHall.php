<?php

namespace App\Models\Game;


use Illuminate\Database\Eloquent\Model;


class ClanHall extends Model
{
    protected $connection = 'game';

    public $timestamps = false;
    public $table = "clanhall";

    public function clan()
    {
        return $this->belongsTo('App\Models\Game\ClanData', 'ownerId', 'clan_id');
    }
}
