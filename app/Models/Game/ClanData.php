<?php

namespace App\Models\Game;


use Illuminate\Database\Eloquent\Model;


class ClanData extends Model
{
    protected $connection = 'game';

    public $timestamps = false;
    public $table = "clan_data";

    protected $primaryKey = 'clan_id';
    public $incrementing = false;

    public function castle()
    {
        return $this->belongsTo('App\Models\Game\Castle', 'hasCastle', 'id');
    }

    public function clanHall()
    {
        return $this->hasOne('App\Models\Game\ClanHall', 'ownerId', 'clan_id');
    }

    public function characters()
    {
        return $this->hasMany('App\Models\Game\Character', 'clanid', 'clan_id');
    }




}
