<?php

namespace App\Models\Game;


use Illuminate\Database\Eloquent\Model;


class Character extends Model
{
    const ONLINE        = 1;
    const OFFLINE_TRADE = 2;

    protected $connection = 'game';

    public $timestamps = false;
    public $table = "characters";

    protected $primaryKey = 'charId';
    public $incrementing = false;

    public function clan()
    {
        return $this->belongsTo('App\Models\Game\ClanData', 'clanid', 'clan_id');
    }


}
