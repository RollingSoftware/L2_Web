<?php

namespace App\Models\Game;


use Illuminate\Database\Eloquent\Model;


class Castle extends Model
{
    protected $connection = 'game';

    public $timestamps = false;
    public $table = "castle";

    public function clan()
    {
        return $this->hasOne('App\Models\Game\ClanData', 'hasCastle', 'id');
    }

}
