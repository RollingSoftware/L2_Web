<?php

namespace App\Models\Game;


use Illuminate\Database\Eloquent\Model;


class RaidBossSpawnList extends Model
{
    protected $connection = 'game';

    public $timestamps = false;
    public $table = "raidboss_spawnlist";

    protected $primaryKey = 'boss_id';
    public $incrementing = false;

    public function stats()
    {
        return $this->hasOne('App\Models\Web\RaidBossStats', 'boss_id', 'boss_id');
    }

}
