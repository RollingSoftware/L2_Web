<?php
namespace App\Models;


use Genert\BBCode\BBCode;

/**
 * Created by PhpStorm.
 * User: Romaniuk.O
 * Date: 28.08.2018
 * Time: 13:49
 */
class myBBCode
{
    private $bbCode;

    public function __construct()
    {
        $this->bbCode = new BBCode;
    }

    public function add()
    {
        $this->bbCode->addParser(
            'image_size',
            '/\[img=(.*?)x(.*?)\](.*?)\[\/img\]/s',
            '<img src="$3" height="$1" width="$2">',
            '$3'
        );

        return $this->bbCode;
    }

}