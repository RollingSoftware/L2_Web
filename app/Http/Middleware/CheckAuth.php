<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            return redirect("/" . App::getLocale() . '/login');
        }


        if(Auth::user()['accessLevel'] < 0){

            Auth::logout();
            return back()->with('errors', collect(['User is Blocked.']));
        }

        return $next($request);
    }
}
