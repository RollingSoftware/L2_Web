<?php

namespace App\Http\Middleware;

use App\Models\Web\Advertising;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Arr;

class GlobalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $locale = $request->segment(1);

        if ($request->has('come_from')) {
            $uniq = false;

            if (!Arr::has($_COOKIE, 'come_from') || Arr::get($_COOKIE, 'come_from') === '') {
                setcookie('come_from', $request->input('come_from'), Carbon::now()->copy()->addYears(2)->getTimestamp());
                $uniq = true;
            }
            $advertising = Advertising::firstOrNew(['name' => $request->get('come_from')]);
            $advertising->view += 1;
            if ($uniq === true) $advertising->unique_view += 1;
            $advertising->save();

            return redirect()->route(Route::currentRouteName(), ['locale' => $locale]);
        } else if (!Arr::has($_COOKIE, 'come_from') || Arr::get($_COOKIE, 'come_from') === '') {            
            setcookie('come_from', 'unknown', Carbon::now()->copy()->addYears(2)->getTimestamp());
            $advertising = Advertising::firstOrNew(['name' => 'unknown']);
            $advertising->view += 1;
            $advertising->unique_view += 1;
            $advertising->save();
        }

        if ($locale != 'ru' && $locale != 'en') {
            $locale = 'en';
            return redirect()->route('home', ['locale' => $locale]);
        }

        App::setLocale($locale);
        $response = $next($request);
        return $response;
    }
}
