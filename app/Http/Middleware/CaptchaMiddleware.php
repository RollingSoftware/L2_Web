<?php

namespace App\Http\Middleware;

use App\Repositories\ThirdParty\ReCaptchaRepository;
use Closure;
use Illuminate\Support\Facades\Log;

class CaptchaMiddleware
{

    private $reCaptchaRepository;

    public function __construct(ReCaptchaRepository $reCaptchaRepository)
    {
        $this->reCaptchaRepository = $reCaptchaRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $recaptcha = $request->get('g-recaptcha-response');

        if($recaptcha)
        {
            $data = [
                'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
                'response' => $recaptcha,
                'remoteip' => $request->ip()
            ];

            $result = $this->reCaptchaRepository->googleCaptcha($data);

            if ($result->has('error-codes'))
            {
                return back()->with('errors', collect(['reCAPTCHA error']));;
            }

            if (!$result->get('success'))
            {
               return back()->with('errors', collect(['reCAPTCHA error']));;
            }

            return $next($request);
        }

        return back()->with('errors', collect(['reCAPTCHA error']));;
    }

}
