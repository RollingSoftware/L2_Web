<?php
namespace MyApp\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class HttpsProtocol {

    public function handle($request, Closure $next)
    {dd($request->secure(), App::environment());
        if (!$request->secure() && App::environment() === 'prod') {
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}