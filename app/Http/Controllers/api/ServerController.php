<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Services\Api\Server\ServerService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ServerController extends Controller
{
    private $serverService;

    public function __construct(ServerService $serverService)
    {
        $this->serverService = $serverService;
    }

    public function info()
   {

       $result = $this->serverService->info();

       $data = [
           'code' => 200,
           'data' => $result
       ];

       return response()->json($data, Response::HTTP_OK);
   }

    public function currentOnlineFile()
    {
        $onlinePlayers = $this->serverService->currentOnlineFile();
        return response($onlinePlayers, 200, [
            'Content-Type' => 'text/plain',
            'Content-Disposition' => 'attachment; filename="online.txt"',
        ]);
    }

    public function unstuck($id) {
        $user = Auth::user();
        $result = $this->serverService->unstuck($user, $id);

        if($result === true){
            $data['code'] = Response::HTTP_OK;
        } else {
            $data['code'] = Response::HTTP_BAD_REQUEST;
        }

        $data['data'] = $result;

        return response()->json($data, Response::HTTP_OK);
    }

}
