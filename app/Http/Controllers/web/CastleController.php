<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Game\CastleRepository;
use App\Repositories\Game\ClanHallRepository;
use Illuminate\Http\Request;

class CastleController extends Controller
{

    private $castleRepository;
    private $clanHallRepository;

    public function __construct(CastleRepository $castleRepository, ClanHallRepository $clanHallRepository)
    {
        $this->castleRepository = $castleRepository;
        $this->clanHallRepository = $clanHallRepository;
    }

    public function index(Request $request)
    {
        $castles = $this->castleRepository->getAllCastles();
        $clanhalls = $this->clanHallRepository->getAllClanHalls();

        return view('web.castle')->with('castles', $castles)->with('clanhalls', $clanhalls);

    }



}