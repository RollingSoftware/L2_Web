<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Game\RaidBossRepository;
use Illuminate\Http\Request;

class RaidBossController extends Controller
{

    private $raidBossRepository;

    public function __construct(RaidBossRepository $raidBossRepository)
    {
        $this->raidBossRepository = $raidBossRepository;
    }

    public function index(Request $request)
    {

        $raidBosses = $this->raidBossRepository->getAllRB();


        return view('web.raidBoss')->with('raidBosses', $raidBosses);
    }

}
