<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Web\Advertising;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class CustomController extends Controller
{

    public function index()
    {
        $names = [
            'etronus',
            'Aproximo'
        ];
        if(in_array (Auth::user()->login, $names, true)) {
            $data = collect(Advertising::all());
            return view('web.custom')->with('data', $data);
        }
        return redirect()->route('account', ['locale' => App::getLocale()]);
    }


}
