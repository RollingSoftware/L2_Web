<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Game\CharacterRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{

    private $characterRepository;

    public function __construct(CharacterRepository $characterRepository)
    {
        $this->characterRepository = $characterRepository;
    }

    public function info(Request $request)
    {

        $characters = $this->characterRepository->getInfo(Auth::user()->login);

        return view('web.accountInfo')->with('characters', $characters);

    }

}
