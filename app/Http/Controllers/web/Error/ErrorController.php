<?php

namespace App\Http\Controllers\web\Error;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ErrorController extends Controller
{
    public function handler(Request $request)
    {
        $code = $request->input('code');

        switch($code){
            case 404:
                return response()->view('errors.404', [], 404);
                break;
            default:
                return response()->view('errors.500', [], 500);
        }

    }
}
