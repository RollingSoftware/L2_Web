<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\myBBCode;
use App\Repositories\Web\ArticleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function redirect()
    {
        return redirect()->route('home', ['locale' => App::getLocale()]);
    }

    public function news(Request $request)
    {
        $bbCode = (new myBBCode())->add();

        $articles = $this->articleRepository->findAllByLocale();

        return view('web.news')->with('articles', $articles)->with('bbCode', $bbCode);

    }

    public function language(Request $request)
    {
        return back();
    }


}
