<?php

namespace App\Http\Controllers\web\Auth;

use App\Http\Controllers\Controller;
use App\Models\Login\Account;
use App\Models\Web\PasswordReset;
use App\Repositories\Login\AccountRepository;
use App\Repositories\Web\PasswordResetRepository;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;


    protected $passwordResetRepository;
    protected $accountRepository;

    protected function redirectTo()
    {
        return route('account', App::getLocale());
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PasswordResetRepository $passwordResetRepository, AccountRepository $accountRepository)
    {
        $this->middleware('guest');
        $this->passwordResetRepository = $passwordResetRepository;
        $this->accountRepository = $accountRepository;
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset', ['locale' => App::getLocale()])->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|alpha_num|min:6|max:16|confirmed',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        $token = $this->passwordResetRepository->findByToken($request->get('token'));

        if($token->isEmpty() || $token->get('status') != PasswordReset::ACTIVE)
        {
            return back()->with('errors', collect(['Token not valid']));
        }

        $user = $this->accountRepository->findByUsernameAndEmail($token->get('username'), $token->get('email'));

        $password = $request->get('password');

        $this->accountRepository->update($token->get('username'), ['password' => Account::passwordHash($password)]);

        $this->passwordResetRepository->deactivate($request->get('token'));

        auth()->login($user);

        return redirect()->route('account', App::getLocale());
    }

}
