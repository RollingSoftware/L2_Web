<?php

namespace App\Http\Controllers\web\Auth;

use App\Http\Controllers\Controller;
use App\Models\Login\Account;
use App\Repositories\Login\AccountRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ChangePwdController extends Controller
{

    private $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function index(Request $request)
    {

        return view('auth.passwords.change');

    }

    public function change(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password_old' => 'required|alpha_num',
            'password' => 'required|alpha_num|min:6|max:16|confirmed'
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!Hash::check($request->get('password_old'), Auth::user()->password))
        {
            return back()->with('errors', collect(['Wrong old password']));
        }

        $this->accountRepository->update(Auth::user()->login, ['password' => Account::passwordHash($request->get('password'))]);

        return redirect("/" . App::getLocale().'/user/account')->with('messages', ['Password was update']);
    }
}
