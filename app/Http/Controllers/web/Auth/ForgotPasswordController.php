<?php

namespace App\Http\Controllers\web\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\Login\AccountRepository;
use App\Repositories\Web\PasswordResetRepository;
use App\Services\EmailService;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    protected $accountRepository;
    protected $passwordResetRepository;
    protected $emailService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AccountRepository $accountRepository, PasswordResetRepository $passwordResetRepository, EmailService $emailService)
    {
        $this->middleware('guest');

        $this->accountRepository = $accountRepository;
        $this->passwordResetRepository = $passwordResetRepository;
        $this->emailService = $emailService;
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $validator = $this->validateEmail($request);
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        $user = $this->accountRepository->findByUsernameAndEmail($request->get('username'), $request->get('email'));

        if(collect($user)->isEmpty())
        {
            return back()->with('errors', collect(['wrong username or email']));
        }

        $token = $this->passwordResetRepository->create($request->get('username'), $request->get('email'));

        $this->emailService->forgotPwd($token->get('token'), $request->get('email'));

        return redirect("/" . App::getLocale() . '/login')->with('messages', ['We have e-mailed your password reset link!']);
    }

    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        return Validator::make($request->all(), ['email' => 'required|email',
                                   'username' => 'required|alpha_num|min:4|max:14']);
    }
}
