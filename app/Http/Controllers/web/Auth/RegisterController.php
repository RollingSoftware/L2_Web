<?php

namespace App\Http\Controllers\web\Auth;

use App\Models\Login\Account;
use App\Models\Web\Advertising;
use App\Models\Web\RegistrationToken;
use App\Repositories\Login\AccountRepository;
use App\Repositories\Web\RegistrationTokenRepository;
use App\Http\Controllers\Controller;
use App\Services\EmailService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Arr;
use Carbon\Carbon;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    private $registrationTokenRepository;
    private $accountRepository;
    private $emailService;



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RegistrationTokenRepository $registrationTokenRepository, AccountRepository $accountRepository, EmailService $emailService)
    {
        $this->middleware('guest');
        $this->registrationTokenRepository = $registrationTokenRepository;
        $this->accountRepository = $accountRepository;
        $this->emailService = $emailService;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|alpha_num|min:4|max:14|unique:login.accounts,login',
            'email' => 'required|string|email|max:50',
            'password' => 'required|alpha_num|min:6|max:16|confirmed'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Account
     */
    protected function create(array $data)
    {
        $result = $this->accountRepository->create($data['name'], $data['email'], $data['password'], App::getLocale());

        $token = $this->registrationTokenRepository->create($data['name']);

        if(Arr::has($_COOKIE, 'come_from') && Arr::get($_COOKIE, 'come_from') != '')
        {
            $advertising = Advertising::firstOrNew( ['name' => Arr::get($_COOKIE, 'come_from')]);
            $advertising->registered += 1;
            $advertising->save();
        }
        
        $this->emailService->registrationConfirm($token->token, $data['name'], $data['email']);

        return $result;
    }

    protected function redirectTo()
    {
        return "/" . App::getLocale() . '/login';
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('messages', ['Thank you for registration, we will send you an email with confirmation']);
    }

    public function reSendActivateLinkForm()
    {
        return view('auth.resendActivateLink');
    }

    public function reSendActivateLink(Request $request)
    {
        $this->validate($request, ['email' => 'required|email',
            'username' => 'required|alpha_num|min:4|max:14']);

        $user = $this->accountRepository->findByUsernameAndEmail($request->get('username'), $request->get('email'));

        if(collect($user)->isEmpty())
        {
            return back()->with('errors', collect(['wrong username or email']));
        }

        $token = $this->registrationTokenRepository->create($request->get('username'));

        $this->emailService->registrationConfirm($token->token, $request->get('username'), $request->get('email'));

        return redirect("/" . App::getLocale().'/login')->with('messages', ['We have e-mailed your activate link!']);
    }

    public function activateUser($locale, string $token)
    {
        try {
            $userToken = $this->registrationTokenRepository->findByToken($token);
            if ($userToken->isEmpty()) {
                return redirect($this->redirectPath())->with('errors', collect(["token_dose_not_exist"]));
            }

            if ($userToken->get('status') == RegistrationToken::USED) {
                return redirect($this->redirectPath())->with('errors', collect(["account_already_active"]));
            }

            if($userToken->get('expire_date') < Carbon::now('utc')->toDateTimeString())
            {
                return "The token is not valid."; // Now it's not working
            }

            $this->accountRepository->activateAccount($userToken->get('username'));
            $this->registrationTokenRepository->changStatus($token);
            $user = $this->accountRepository->getByUsername($userToken->get('username')); 
            auth()->login($user);
        } catch (\Exception $exception) {
            logger()->error($exception);
            return "Whoops! something went wrong. - ".$exception->getMessage();
        }
        return redirect()->to("/" . App::getLocale().'/user/account');
    }
}
