<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\Login;


use App\Models\Login\Account;


class AccountRepository
{
   public function getByUsername($username)
   {
       return Account::where('login', $username)->first();
   }

   public function activateAccount($username)
   {
       Account::where('login', $username)->update(['accessLevel' => 0]);
   }

   public function create($login, $email, $password, $language)
   {
       $result = Account::create([
           'login' =>$login,
           'email' => $email,
           'password' => Account::passwordHash($password),
           'accessLevel' => Account::BLOCKED,
           'language' => $language
       ]);

       return $result;
   }

   public function findByUsernameAndEmail($username, $email)
   {
       $result = Account::where('login', $username)->where('email', $email)->first();

       return $result;
   }
   
   public function update($username, $data)
   {
       Account::where('login', $username)->update($data);
   }

}