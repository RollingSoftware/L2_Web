<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\ThirdParty;


use App\Services\Api\Client;


class ReCaptchaRepository
{

    protected $client;

    /**
     * UserRepository constructor.
     * @param $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function googleCaptcha($data)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';

        $data = $this->client->post($url, $data);

        return collect($data);
    }
}