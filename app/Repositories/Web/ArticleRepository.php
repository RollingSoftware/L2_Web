<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\Web;


use App\Models\Web\Article;
use Illuminate\Support\Facades\App;


class ArticleRepository
{
    public function findAllByLocale()
    {
        $result = Article::where('lang', App::getLocale())->OrderBy('updated', 'DESC')->get();
        return collect($result);
    }
}