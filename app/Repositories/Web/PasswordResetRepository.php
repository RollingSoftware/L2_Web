<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\Web;


use App\Models\Web\PasswordReset;

class PasswordResetRepository
{
    public function findByToken($token)
    {
        $result = PasswordReset::where('token', $token)->first();

        return collect($result);
    }

    public function deactivate($token)
    {
        PasswordReset::where('token', $token)->update(['status' => PasswordReset::USED]);
    }

    public function create($username, $email, $status = PasswordReset::ACTIVE)
    {

        $token = PasswordReset::updateOrCreate(['username' => $username, 'email' => $email], ['token' => bin2hex(openssl_random_pseudo_bytes(32)), 'status' => $status]);

        return collect($token);

    }

}