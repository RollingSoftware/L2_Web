<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\Web;


use App\Models\Web\RegistrationToken;
use Carbon\Carbon;


class RegistrationTokenRepository
{
    public function findByToken($token)
    {
        $result = RegistrationToken::where('token', $token)->first();

        return collect($result);
    }

    public function changStatus($token)
    {
        $result = RegistrationToken::where('token', $token)->update(['status' => RegistrationToken::USED]);

        return collect($result);
    }

    public function create($username)
    {
        $token = RegistrationToken::create([
            'token' => bin2hex(openssl_random_pseudo_bytes(32)),
            'expire_date' => Carbon::now('utc')->addSeconds(RegistrationToken::TOKEN_VALID),
            'status' => RegistrationToken::ACTIVE,
            'username' => $username
        ]);

        return $token;
    }


}