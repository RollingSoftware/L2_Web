<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\Api;

use App\Models\Game\Character;
use App\Models\Game\CustomVariable;

class ServerRepository
{

    public function countCharacters($type)
    {
        return Character::where('online', $type)->count();
    }

    public function getCustomVariables($variable)
    {
        return collect (CustomVariable::where('var', $variable)->first());
    }

}