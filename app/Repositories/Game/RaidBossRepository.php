<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\Game;

use App\Models\Game\RaidBossSpawnList;

class RaidBossRepository
{

    public function getAllRB()
    {
            $result = RaidBossSpawnList::join('l2web.raidboss_stats', 'raidboss_spawnlist.boss_id', '=', 'raidboss_stats.boss_id')->orderBy('raidboss_stats.level', "DESC")
                ->select('raidboss_spawnlist.*', 'raidboss_stats.name', 'raidboss_stats.level')
                ->get();

            return collect($result);

    }

}