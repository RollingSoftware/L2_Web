<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\Game;


use App\Models\Game\Castle;


class CastleRepository
{

    public function getAllCastles()
    {
        $result = Castle::with('clan')->get();

        return collect($result);

    }

}