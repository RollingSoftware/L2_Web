<?php

/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\Game;


use App\Models\Game\Character;


class CharacterRepository
{

  public function getInfo($login)
  {
    $result = Character::with('clan')->where('account_name', $login)->get();

    return collect($result);
  }

  public function findByAccoundAndId($user, $id)
  {
    return collect(Character::where('charId', $id)->where('account_name', $user)->first());
  }

  public function updateById($id, $data)
  {
    Character::where('charId', $id)->update($data);
  }
}
