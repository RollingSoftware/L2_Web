<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/11/18
 * Time: 8:07 PM
 */

namespace App\Repositories\Game;


use App\Models\Game\ClanHall;


class ClanHallRepository
{

    public function getAllClanHalls()
    {
        $result = ClanHall::with('clan')->get();

        return collect($result);

    }

}