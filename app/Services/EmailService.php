<?php
namespace App\Services;

use App\Mail\ForgotPwd;
use App\Mail\RegistrationConfirm;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class EmailService
{
    use Queueable, SerializesModels;


    public function registrationConfirm($token, $name, $toEmail)
    {
        $mail = new RegistrationConfirm($token, $name);
        $this->send($mail, $toEmail);
    }

    public function forgotPwd($token, $toEmail)
    {
        $mail = new ForgotPwd($token);
        $this->send($mail, $toEmail);
    }

    private function send($message, $to)
    {

        try
        {
            Mail::to($to)->send($message);
        }
        catch(\Exception $e)
        {
            LOG::error($e);
        }

    }
}