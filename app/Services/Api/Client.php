<?php
/**
 * Created by PhpStorm.
 * User: Morris
 * Date: 3/10/18
 * Time: 7:45 PM
 */

namespace App\Services\Api;

use Exception;
use Illuminate\Support\Facades\Log;

class Client
{
    protected $client;

    /**
     * Client constructor.
     * @param $client
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }


    public function get($url, $params = NULL)
    {

        return $this->request('GET', $url, $params);
    }

    public function post($url, $params = NULL, $body = NULL)
    {
        return $this->request('POST', $url, $params, $body);

    }

    public function put($url, $params = NULL, $body = NULL)
    {
        return $this->request('PUT', $url, $params, $body);
    }

    public function patch($url, $params = NULL, $body = NULL)
    {
        return $this->request('PATCH', $url, $params, $body);

    }

    private function request($method, $url, $params = NULL, $jsonParams = NULL)
    {
        $options = [
            'read_timeout' => 10,
            'exceptions' => false,
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];

        $options['headers'] = $headers;


        if($params)
        {
            $options['query'] = $params;
        }

        if($jsonParams)
        {
            $options['body'] = json_encode($jsonParams);

        }

        Log::info(var_export($options, true));

        $response = $this->client->request($method, $url, $options);

        return $this->response($response);
    }

    private function response($response)
    {
        $body = $response->getBody()->getContents();

        $statusCode = $response->getStatusCode();

        Log::info(var_export($body, true));


        $data = json_decode($body, true);

        Log::info(var_export($data, true));

        if (json_last_error() !== JSON_ERROR_NONE)
        {
            throw new Exception('Json error - '. json_last_error());
        }

        return $data;
    }
}