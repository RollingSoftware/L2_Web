GRANT ALL PRIVILEGES ON `l2web`.* TO 'web'@'localhost';
GRANT SELECT ON `l2jgs`.`custom_variables` TO 'web'@'localhost';
GRANT SELECT ON `l2jgs`.`clan_data` TO 'web'@'localhost';
GRANT SELECT ON `l2jgs`.`clanhall` TO 'web'@'localhost';
GRANT SELECT ON `l2jgs`.`raidboss_spawnlist` TO 'web'@'localhost';
GRANT SELECT ON `l2jgs`.`castle` TO 'web'@'localhost';
GRANT SELECT, UPDATE ON `l2jgs`.`characters` TO 'web'@'localhost';
GRANT SELECT, INSERT, UPDATE ON `l2jls`.`accounts` TO 'web'@'localhost';
