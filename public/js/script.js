$(document).ready(function() {
    $.getJSON("/server/info", function(data) {

        if(data.code == 200) { 
            data = data.data
        } else {
            return NULL;
        }


        var color = 'red';
        if (data.server === 'Online') {
            color = 'green';
        }
        var online_string = "<span class='online-count'>" + data.online_count + "</span>";
        online_string += "<span class='offline-traders-count'> (Off: " + data.offline_traders_count + ")</span>";
        $("#online").html(online_string);
        $("#status").text(data.server).css("color", color);
    });
    updateClock();
    setInterval('updateClock()', 1000);


    $("#banners_container").html(
        '<a href="https://l2argument.ru" target="_blank" title="База знаний и рейтинг серверов Lineage 2" style="position: fixed; right: 0px; bottom: 0px; display: block; z-index: 9999;"><img src="https://l2argument.ru/assets/images/l2argument3.jpg" /></a>' +
        "<a href='https://www.top100arena.com/category/lineage2?vote=98914' title='Bloodshed'><img src='https://www.top100arena.com/hit/98914/small' alt='Lineage 2 private server' width='88px' height='31px'></a>" +
        '<a href="https://la2.mmotop.ru/en/servers/35702/votes/new" target="_blank"><img src="http://img.mmotop.ru/mmo_35702.png" border="0" id="mmotopratingimg" alt="Рейтинг серверов mmotop"></a>' +
        '<a href="https://l2topzone.com/vote/id/18229" target="_blank" title="l2topzone" ><img src="https://l2topzone.com/vb/l2topzone-Lineage2-vote-banner-bottom-left-2.png" style="position: fixed; z-index:99999; bottom: 0; left: 0;" alt="l2topzone.com" border="0"></a>'
    );
});

function unstuck(id) {
    $.getJSON("/server/unstuck/" + id, function(data) {
        if(data.code === 200) {
            var online_string = "<li>Unstuck success</li>";
            $(".messages-box").html(online_string);
        } else {
            var online_string = "<li>" + data.data + "</li>";
            $(".flash-box").html(online_string);
        }
    });
}

function updateClock() {
    var momentTime = moment($("#time").text(), 'HH:mm:ss');
    $("#time").text(momentTime.add(1, 'seconds').format('HH:mm:ss'));
}
