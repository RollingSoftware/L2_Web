<?php

return [

    'News' => 'News',
    'About' => 'About',
    'Discord' => 'Discord',
    'Downloads' => 'Downloads',
    'Account' => 'Account',
    'Start Playing' => 'Start Playing',
    'Castles' => 'Castles',
    'Raid Bosses' => 'Raid Bosses',
];