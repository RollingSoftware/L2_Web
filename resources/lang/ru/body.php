<?php

return [

    'news' => [
        'No news available' => 'Нету новостей',
    ],


    'Download the client from' => 'Скачать клиент',
    'English Patch' => 'Английский Патч',
    'Russian Patch' => 'Русский Патч',
    'Register your account' => 'Зарегистрируйте свой аккаунт',
    'here' => 'здесь',
    'Launch the game through \'bloodshed-en/l2.exe\' or \'bloodshed-ru/l2.exe\' depending on your patch.' => 'Запускайте игру через \'bloodshed-en/l2.exe\' или \'bloodshed-ru/l2.exe\', зависимо от вашего патча.',
    'Create your character, find some friends and have fun!' => 'Создавайте своего персонажа, найдите друзей и веселитесь!',
    'Download and unpack into the game folder one of the following patches' => 'Скачайте и раcпакуйте в папку с игрой один из патчей',
    'Login' => 'Войти',
    'Forgot Password' => 'Забыли пароль?',
    'Username' => 'Логин',
    'Password' => 'Пароль',
    'Repeat Password' => 'Повторите пароль',
    'Remind Me' => 'Напомнить',
    'Register' => 'Зарегистрироваться',
    'Change Password' => 'Сменить пароль',
    'Logout' => 'Выйти',
    'The reCAPTCHA wasn\'t entered correctly. Go back and try it again' => 'Не верно введена reCAPTCHA, попробуйте еще раз',
    'Incorrect request' => 'Неверный запрос',
    'Could not send password recovery email, please contact the administration' => 'Не удалось отправить письмо для восстановления пароля, пожалуйста свяжитесь с администрацией',
    'Online' => 'В игре',
    'Status' => 'Статус',
    'Mirror' => 'Зеркало',
    'Time' => 'Время',
    'Greetings %s, this is your personal space' => 'Приветствую %s, это ваше личное пространство',
    'Thank you for registration, we will send you an email with confirmation' => 'Спасибо за регистрацию, мы отправили Вам письмо с подтверждением',
    'New password was sent to your email' => 'Новый пароль был отправлен Вам на почту',
    'Your password has been changed successfully' => 'Ваш пароль был изменен',
    'Alive' => 'Жив',
    'Dead' => 'Мертв',
    'Unknown' => 'Неизвестный',
    'Castles and Clan Halls' => 'Замки и Залы Кланов',
    'Clan Halls' => 'Залы Кланов',
    'None' => 'Нету',
    'Owner' => 'Владелец',
    'Character have to be offline' => 'Персонаж должен быть оффлайн',
    'Character' => 'Персонаж',
    'pk' => 'пк',
    'pvp' => 'пвп',
    'karma' => 'карма',
    'sp' => 'сп',
    'fame' => 'слава',
    'clan' => 'клан',
    'Hello' => 'Здравствуй',
    'Bad credentials.' => 'Неверные учетные данные',
    'Old password' => 'Старый пароль',
    'New password' => 'Новый пароль',
    'Reset Password' => 'Востановить пароль',
    'Password was update' => 'Пароль успешно сменен',
    'Account' => 'Пользователь',
    'Start Playing' => 'Начать играть',
    'These credentials do not match our records.' => 'Введен неверный логин или пароль',
    'The name must be at least 4 characters.' => 'Логин должен быть минимум 4 символа',
    'The password must be at least 6 characters.' => 'Пароль должен быть минимум 6 символов',
    'The password confirmation does not match.' => 'Пароль не совпадает',
    'The password may not be greater than 16 characters.' => 'Пароль должен быть максимум 16 символов',
    'The name may not be greater than 14 characters.' => 'Логин должен быть максимум 14 символа',
    'The name has already been taken.' => 'Такой логин уже существует',
    'User is Blocked' => 'Пользователь не активный',
    'Remember Me' => 'Запомнить меня',
    'We have e-mailed your password reset link!' => 'Мы отправили вам ссылку на восстановления пароля по почте',
    'reCAPTCHA error' => 'Не верно введена reCAPTCHA, попробуйте еще раз',
    'User is Blocked.' => 'Пользователь заблокирован',
    'active_account' => 'Не пришел код активации?',
    'active_account_url' => 'Выслать повторно',
    'Forgot Password_url' => 'Восстановить',
    'token_dose_not_exist' => 'Токе не существует',
    'account_already_active' => 'Акаунт уже актевирован',
    'We have e-mailed your activate link!' => 'Мы отправили Вам письмо с подтверждением'
];