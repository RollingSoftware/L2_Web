<?php

return [

    'News' => 'Новости',
    'About' => 'О сервере',
    'Discord' => 'Discord',
    'Downloads' => 'Скачать',
    'Account' => 'Пользователь',
    'Start Playing' => 'Начать играть',
    'Castles' => 'Замки',
    'Raid Bosses' => 'Боссы',

];