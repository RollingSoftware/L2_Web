@extends('index')

@section('content')

    <h2 class="center-text">{{trans('header.News')}}</h2>
    <hr/>

    @if ($articles->isEmpty())
    <h3 class="center-text">{{trans('body.news.No news available')}}</h3>
    @else
    @foreach ($articles->all() as $article)
    <div class="article">
        <div class="article-header">
            <h3 class="inline article-title">{!! $bbCode->convertToHtml($article->title) !!}</h3>
            <div class="inline article-date">{{ Carbon\Carbon::parse($article->created)->format('Y-m-d') }}</div>
        </div>

        <div class="article-content">{!! $bbCode->convertToHtml($article->content) !!}</div>
        <hr/>
    </div>
    @endforeach
    @endif


@endsection
