@extends('index')

@section('content')

    <h2 class="center-text">{{trans('header.Start Playing')}}</h2>
    <hr/>

    <ol>
        <li><p>{{trans('body.Download the client from')}} Lineage 2 HF <a href="{{ env('CLIENT_URL_MAIN') }}" target="_blank"><u>{{trans('body.Mirror')}} 1</u></a>, <a href="{{ env('CLIENT_URL_SECONDARY') }}" target="_blank"><u>{{trans('body.Mirror')}} 2</u></a></p></li>
        <li>
            <p>{{trans('body.Download and unpack into the game folder one of the following patches')}}:
            <ul>
                <li>{{ trans('body.English Patch')}} <a href="{{ env('EN_PATCH_MAIN') }}" target="_blank"><u>{{trans('body.Mirror')}} 1</u></a>, <a href="{{ env('EN_PATCH_SECONDARY') }}" target="_blank"><u>{{trans('body.Mirror')}} 2</u></a></li>
                <li>{{trans('body.Russian Patch')}} <a href="{{ env('RU_PATCH_MAIN') }}" target="_blank"><u>{{trans('body.Mirror')}} 1</u></a>, <a href="{{ env('RU_PATCH_SECONDARY') }}" target="_blank"><u>{{trans('body.Mirror')}} 2</u></a></li>
            </ul>
            </p>
        </li>
        <li><p>{{trans('body.Register your account')}} <a href="{{ route('register', ['locale' => App::getLocale()]) }}"><u>{{trans('body.here')}}</u></a></p></li>
        <li><p>{{trans("body.Launch the game through 'bloodshed-en/l2.exe' or 'bloodshed-ru/l2.exe' depending on your patch.")}}</p></li>
        <li><p>{{trans("body.Create your character, find some friends and have fun!")}}</p></li>
    </ol>

@endsection
