@extends('index')

@section('content')

    <h2 class="center-text">{{trans("header.Raid Bosses")}}</h2>
    <hr>
    <center>
        <table>
            <tbody>
            @foreach ($raidBosses as $rb)
            <tr>
                <td>
                    @if ($rb->name)
                    <b>{{"[{$rb->level}]"}}</b> {{$rb->name}}
                    @else
                    {{trans('bode.Unknown')}}
                    @endif
                </td>
                <td>
                    @if ($rb->respawn_time != 0)
                    <span style="color:red">{{trans("body.Dead")}}</span>
                    @else
                    <span style="color:green">{{trans("body.Alive")}}</span>
                    @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </center>

@endsection
