@extends('index')

@section('content')

    <h2 class="center-text">{{trans("body.Castles and Clan Halls")}}</h2>
    <hr>

    <center>
        <h3>{{trans("header.Castles")}}</h3>

        @foreach ($castles->all() as $castle)
            <div class="row">
                <div class="column" style="text-align:right"><b>{{ $castle->name }}</b></div>

                <div class="column" style="text-align:left">{{trans("body.Owner")}}:

                    @if ($castle->clan)
                    <span style="color:red">{{$castle->clan->clan_name}}</span>
                    @else
                    <span style="color:green">{{trans("body.None")}}</span>
                    @endif
                </div>
            </div>
        @endforeach

        <h3>{{trans("body.Clan Halls")}}</h3>

        @foreach ($clanhalls->all() as $ch)
        <div class="row">
            <div class="column" style="text-align:right"><b>{{ $ch->name }}</b></div>

            <div class="column" style="text-align:left">{{trans("body.Owner")}}:
                @if ($ch->clan)
                <span style="color:red">{{$ch->clan->clan_name}}</span>
                @else
                <span style="color:green">{{trans("body.None")}}</span>
                @endif
            </div>
        </div>
        @endforeach
    </center>
    <hr/>

@endsection
