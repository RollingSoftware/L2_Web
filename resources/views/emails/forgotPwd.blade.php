<!doctype html>
<html>
<head>
  <style>
    .button{
      cursor: pointer;
      background-color: #008CBA;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      margin: 4px 2px;
      -webkit-transition-duration: 0.4s;
      transition-duration: 0.4s;
      border: 2px solid #008CBA;
      border-radius: 43px;
      font-family: gt-eesti,Helvetica,Arial,sans-serif;
      box-sizing: border-box;
      padding: 14px 24px;
    }
  </style>
</head>

<body style="text-align: center; font-family: gt-eesti,Helvetica,Arial,sans-serif">
<div class="block" style="display: block; margin-left: auto; margin-right: auto;">
  <h2>Hello!</h2>
  <p>You are receiving this email because we received a password reset request for your account.</p>

  <a class="button" style="color: #ffffff;" href="{{ route('password.reset', $token) }}">Reset Password
  </a>

  <p>If you did not request a password reset, no further action is required.</p>
  <div style="text-align: left;">

    <p>Regards,</p>
    <a href="{{ env('APP_URL') }}">{{env('APP_NAME')}}</a>
  </div>
  <hr>
  <p style="font-size: 0.7em; color: inherit">
    If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below into your web browser: {{ route('password.reset', $token) }}
  </p>
</div>
</body>
</html>
