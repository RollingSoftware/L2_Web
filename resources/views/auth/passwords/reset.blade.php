@extends('auth.account')

@section('account_content')
<div class="container">
    <form method="POST" action="{{ route('post.password.reset' , ['locale' => App::getLocale()]) }}">
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="center-text">
            <div class="border-pane">
                <label for="password">Password</label>
                <input id="password" type="password" name="password" required>
                <label for="password-confirm">Confirm Password</label>
                <input id="password-confirm" type="password" name="password_confirmation" required>
            </div>
            <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
            <button type="submit">{{trans('body.Reset Password')}}</button>
        </div>
        {{ csrf_field() }}
    </form>

@endsection
