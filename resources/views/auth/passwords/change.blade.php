@extends('auth.account')

@section('account_content')
<div class="container">
    <form method="POST" action="{{ route('post.password.change' , ['locale' => App::getLocale()]) }}">
        <div class="center-text">
            <div class="border-pane">
                <label for="password_old">{{trans('body.Old password')}}</label>
                <input id="password_old" type="password" name="password_old" required>
                <label for="password">{{trans('body.New password')}}</label>
                <input id="password" type="password" name="password" required>
                <label for="password-confirm">{{trans('body.Repeat Password')}}</label>
                <input id="password-confirm" type="password" name="password_confirmation" required>
            </div>
            <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
            <button type="submit">{{trans('body.Reset Password')}}</button>
        </div>
        {{ csrf_field() }}
    </form>

@endsection
