@extends('auth.account')

@section('account_content')

    <div class="panel-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('post.reSendActivateLink', ['locale' => App::getLocale()]) }}">
            <div class="center-text">
                <div class="border-pane">
                    <label for="email">E-Mail Address</label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                    <label for="email">Username</label>
                    <input id="username" type="text" name="username" value="{{ old('username') }}" required>
                </div>
                <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                <button type="submit">{{trans('body.Remind Me')}}</button>
            </div>
            {{ csrf_field() }}
        </form>
    </div>


@endsection
