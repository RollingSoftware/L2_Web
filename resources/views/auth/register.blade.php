@extends('auth.account')

@section('account_content')
    <form  method="POST" action="{{ route('register', ['locale' => App::getLocale()])}}">
        {{ csrf_field() }}
        <div class="center-text">
            <div class="border-pane">

                <label for="login">{{trans('body.Username')}}</label>

                <div >
                    <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>
                </div>

                <label for="email">E-Mail</label>

                <div>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                </div>

                <label for="password">{{trans('body.Password')}}</label>

                <div>
                    <input id="password" type="password" name="password" required>
                </div>

                <div>
                    <label for="password-confirm">{{trans('body.Repeat Password')}}</label>

                    <div>
                        <input id="password-confirm" type="password" name="password_confirmation" required>
                    </div>
                </div>
            </div>

             <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>

            <button type="submit">{{trans('body.Register')}}</button>
            <div class='accounts-misc'>
                <p>{{trans('body.active_account')}} <a href="{{ route('reSendActivateLink', ['locale' => App::getLocale()]) }}">{{trans('body.active_account_url')}}</a></p>
                <p>{{trans('body.Forgot Password')}} <a href="{{ route('password.request', ['locale' => App::getLocale()]) }}">{{trans('body.Forgot Password_url')}}</a></p>
            </div>
        </div>
    </form>
@endsection
