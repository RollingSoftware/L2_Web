@extends('index')

@section('content')
    @if (Auth::check())
    <div class="account-menu">
        <a class="account-menu-item @if(Route::getCurrentRoute()->getName() == 'account') active @endif" href="{{ route('account', ['locale' => App::getLocale()]) }}"><h3>{{trans('header.Account')}}</h3></a>
        <div class="account-menu-separator">|</div>
        <a class="account-menu-item  @if(Route::getCurrentRoute()->getName() == 'get.password.change') active @endif" href="{{ route('get.password.change', ['locale' => App::getLocale()]) }}"><h3>{{trans('body.Change Password')}}</h3></a>
        <div class="account-menu-separator">|</div>
        <a class="account-menu-item @if(Route::getCurrentRoute()->getName() == 'logout') active @endif" href="{{ route('logout', ['locale' => App::getLocale()]) }}"><h3>{{trans('body.Logout')}}</h3></a>
    </div>
    @else
    <div class="account-menu">
      <a class="account-menu-item  @if(Route::getCurrentRoute()->getName() == 'login') active @endif" href="{{ route('login', ['locale' => App::getLocale()]) }}"><h3>{{trans('body.Login')}}</h3></a>
      <div class="account-menu-separator">|</div>
      <a class="account-menu-item  @if(Route::getCurrentRoute()->getName() == 'register') active @endif" href="{{ route('register', ['locale' => App::getLocale()]) }}"><h3>{{trans('body.Register')}}</h3></a>
      {{--<div class="account-menu-separator">|</div>--}}
      {{--<a class="account-menu-item  @if(Route::getCurrentRoute()->getName() == 'password.request') active @endif" href="{{ route('password.request', ['locale' => App::getLocale()]) }}"><h3>{{trans('body.Forgot Password')}}</h3></a>--}}
    </div>
    @endif

    <hr/>
    <div class="flash-box">
        @foreach ($errors->all() as $error)
            <li>{{ trans("body.$error") }}</li>
        @endforeach
    </div>
    <div class="messages-box">
        @if( session('messages'))
            @foreach (session('messages') as $message)
                <li>{{ trans("body.$message") }}</li>
            @endforeach
        @endif
    </div>
    <br>
    @yield('account_content')
@endsection
