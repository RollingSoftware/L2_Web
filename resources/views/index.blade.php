<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Lineage 2 Bloodshed">
    <meta name="keywords" content="Lineage 2,Bloodshed,Lineage 2 Bloodshed,L2 Bloodshed,Highfive,Craft-PvP,PvP-Craft,Craft-RB-PvP,highfive x100,highfive pvp">
    <title>Bloodshed</title>
    <link href="{{ url('/css/style.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ url('favicon/l2.jpg') }}">
  </head>

<body>
    <div class="background-container">
      <div class="menu">
        <div class="lang-menu">

            <a href="{{ route(Route::currentRouteName(), ['locale' => 'en']) }}"><img class="lang-item" src="{{url('/img/en-flag.png')}}"/></a>
            <a href="{{ route(Route::currentRouteName(), ['locale' => 'ru']) }}"><img class="lang-item" src="{{url('/img/ru-flag.png')}}"/></a>

            @if(Request::is('login'))
                  <span>Request::is('login')</span>
            @endif
        </div>

          <div class="navigation-menu">
              <a href="{{ route('home', ['locale' => App::getLocale()]) }}"><button class="menu-item @if(Route::getCurrentRoute()->getName() == 'home') active @endif">{{trans('header.News')}}</button></a>
              <a href="{{ route('account', ['locale' => App::getLocale()]) }}"><button class="menu-item @if(Route::getCurrentRoute()->getName() == 'account' || Route::getCurrentRoute()->getName() == 'login') active @endif">{{trans('header.Account')}}</button></a>
              <a href="{{ route('downloads', ['locale' => App::getLocale()]) }}"><button class="menu-item @if(Route::getCurrentRoute()->getName() == 'downloads') active @endif">{{trans('header.Start Playing')}}</button></a>
              <a href="{{ route('about', ['locale' => App::getLocale()]) }}"><button class="menu-item @if(Route::getCurrentRoute()->getName() == 'about') active @endif">{{trans('header.About')}}</button></a>
              <a href="{{ route('castles', ['locale' => App::getLocale()]) }}"><button class="menu-item @if(Route::getCurrentRoute()->getName() == 'castles') active @endif">{{trans('header.Castles')}}</button></a>
              <a href="{{ env('DISCORD_URL') }}" target="_blank"><button class="menu-item">{{trans('header.Discord')}}</button></a>
          </div>
      </div>

      <div class="banners" id="banners_container"></div>

      <div class="container">
        <div class="server-status">
          <div>{{ trans('body.Time') }}: <span id="time">{{ date('H:i:s') }}</span></div>
          <div>
            <span>{{ trans('body.Online') }}: </span><span id="online">Loading...</span></span>
          </div>
          <div>{{ trans('body.Status') }}: <span id="status">Loading...</span></div>
        </div>

        <div class="content">
            @yield('content')
        </div>
      </div>

      <div class="footer"></div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js"></script>
  <script src="{{ url('/js/script.js') }}"></script>
  <script src='https://www.google.com/recaptcha/api.js?hl={{ App::getLocale() }}'></script>
</body>
</html>
